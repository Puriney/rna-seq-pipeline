
*****RNA-seq Pipeline**

- QC of reads by `fastqc`
- Alignment by `STAR`, and regular processing by `samtools`
- Count matrix by `htseq-count` from [HTSeq](http://www-huber.embl.de/users/anders/HTSeq/doc/index.html)
- DE detection by `DESeq2`
- GO analysis by `clusterProfiler`
