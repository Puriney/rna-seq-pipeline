#!/usr/bin/env bash
#$ -S /bin/bash
#$ -cwd
#$ -M yun.yan@nyumc.org
#$ -m e
#$ -l h_vmem=30G
#$ -l mem_token=10G
#$ -N htseq_count

set -e

# use my own htseq under py2.7
source activate py27
SHARED_DB="/ifs/home/yy1533/SHARED_DB"
GTF="$SHARED_DB/gencode/release_M11/gtf/mus_musculus/gencode.vM11.annotation.gtf"

ALNDIR="/ifs/home/yy1533/Projects/challenge/res/bam"
OUTDIR="/ifs/home/yy1533/Projects/challenge/res/count"

if [ ! -d "$OUTDIR" ]; then
    mkdir -p $OUTDIR
fi

date

for f in `ls $ALNDIR/*.sorted.bam`
do
    fname=${f##*/}
    fbasename=${fname%.sorted.bam}
    echo ">>> $fname"

    htseq-count --format=bam $f $GTF > $OUTDIR/${fbasename}.count

    date
done
