#!/usr/bin/env bash
#$ -S /bin/bash
#$ -cwd
#$ -M yun.yan@nyumc.org
#$ -m e
#$ -l h_vmem=30G
#$ -l mem_token=10G
#$ -N sam2bam

set -e

OUTMAPDIR="/ifs/home/yy1533/Projects/challenge/res/bam"

date

for sam in `ls $OUTMAPDIR/*.Aligned.out.sam`
do
    sam_name=${sam##*/}
    sam_basename=${sam_name%.Aligned.out.sam}
    echo ">>> $sam_name"

    samtools view -bS $sam | \
        samtools sort -o $OUTMAPDIR/${sam_basename}.sorted.bam -

    samtools index $OUTMAPDIR/${sam_basename}.sorted.bam $OUTMAPDIR/${sam_basename}.sorted.bai
    date
done
