#!/usr/bin/env bash
#$ -S /bin/bash
#$ -cwd
#$ -M yun.yan@nyumc.org
#$ -m e
#$ -l h_vmem=30G
#$ -l mem_token=10G
#$ -pe threaded 4
#$ -N Build_Genome

set -e

# STAR="/local/apps/star/2.5.0c/bin/Linux_x86_64/STAR"
SHARED_DB="/ifs/home/yy1533/SHARED_DB"
GENOME_INDEX_FPATH="$SHARED_DB/ALIGNER_INDEX/mm10/STAR/gencode"
GENOME_FA="$SHARED_DB/gencode/release_M11/fasta/mus_musculus/GRCm38.primary_assembly.genome.fa"

if [ ! -d "$GENOME_INDEX_FPATH" ]; then
    mkdir -p $GENOME_INDEX_FPATH
fi

date

STAR    --runMode genomeGenerate \
        --genomeDir $GENOME_INDEX_FPATH \
        --genomeFastaFiles $GENOME_FA \
        --runThreadN 3

date
