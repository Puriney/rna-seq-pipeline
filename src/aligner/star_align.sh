#!/usr/bin/env bash
#$ -S /bin/bash
#$ -cwd
#$ -M yun.yan@nyumc.org
#$ -m e
#$ -l h_vmem=30G
#$ -l mem_token=10G
#$ -pe threaded 4
#$ -N Align

set -e

# STAR="/local/apps/star/2.5.0c/bin/Linux_x86_64/STAR"
SHARED_DB="/ifs/home/yy1533/SHARED_DB"
GENOME_INDEX_FPATH="$SHARED_DB/ALIGNER_INDEX/mm10/STAR/gencode"
GTF="$SHARED_DB/gencode/release_M11/gtf/mus_musculus/gencode.vM11.annotation.gtf"
OUTMAPDIR="/ifs/home/yy1533/Projects/challenge/res/bam"
if [ ! -d "$OUTMAPDIR" ]; then
    mkdir -p $OUTMAPDIR
fi

for fa in `ls /ifs/home/yy1533/DATA/challenge/*.gz`
do
    fa_name=${fa##*/}
    fa_basename=${fa_name%.fastq.gz}
    echo $fa_name
    echo $fa_basename
    STAR    --genomeDir $GENOME_INDEX_FPATH \
            --sjdbGTFfile $GTF \
            --readFilesCommand zcat \
            --readFilesIn $fa \
            --runThreadN 4   \
            --outFileNamePrefix "${OUTMAPDIR}/${fa_basename}."  \
            --outReadsUnmapped Fastx
done
